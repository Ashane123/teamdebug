package com.hotel.hotel_management_system;

public class LoggedUser {
    private String type = "Admin";

    private static LoggedUser u = new LoggedUser();

    private LoggedUser(){};

    public static LoggedUser getLoggedUser(){
        return u;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

